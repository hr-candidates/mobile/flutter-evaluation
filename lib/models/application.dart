class Application {
  String field = "";
  String specialization = "";
  String email = "";
  String firstName = "";
  String lastName = "";

  Application();

  Map<String, dynamic> toJson() => {
        "email": email,
        "field": field,
        "first_name": firstName,
        "last_name": lastName,
        "specialization": specialization,
      };
}
