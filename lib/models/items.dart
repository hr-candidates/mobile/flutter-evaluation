// ignore: non_constant_identifier_names
List<Item> ItemsFromJson(dynamic jsonData) => (jsonData != null)
    ? List.from(jsonData.map(
        (value) => Item.fromJson(value),
      ))
    : List.empty();

class Item {
  final String key;
  final String displayValue;

  Item({
    required this.key,
    required this.displayValue,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        key: json["key"],
        displayValue: json["value"],
      );
}
