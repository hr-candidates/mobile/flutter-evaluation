enum Field {
  mobile,
  frontend,
  backend,
  ml,
  data_science,
}

extension Value on Field {
  String get displayValue {
    switch (this) {
      case Field.mobile:
        return "Mobile";
      case Field.frontend:
        return "Frontend Web";
      case Field.backend:
        return "Backend";
      case Field.ml:
        return "Machine Learning";
      case Field.data_science:
        return "Data Science";
    }
  }
}
