import 'package:flutter/material.dart';
import 'package:flutter_evaluation/utils.dart';

class PlaceholderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Spacer(),
          Image(image: AssetImage("assets/logo.png")),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: 64,
              horizontal: 32,
            ),
            child: _PlaceholderPageText(),
          ),
          Spacer(),
        ],
      ),
    );
  }
}

class _PlaceholderPageText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: "Thanks for your interest in Omninext!"),
          TextSpan(
            text: "Clean me out to start building the app",
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
          TextSpan(
            text: "Also remember to make use of 'utils' and 'models'",
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
          TextSpan(
            text: "Good luck!",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ].expand((line) => [line, TextSpan(text: "\n\n")]).toList(),
        style: TextStyle(
          color: PRIMARY_COLOR,
          fontSize: 24,
        ),
      ),
      textAlign: TextAlign.center,
    );
  }
}
