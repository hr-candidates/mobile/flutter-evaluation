import 'package:flutter/material.dart';

const Color PRIMARY_COLOR = Colors.white;
const Color SECONDARY_COLOR = Color(0xFF00a9e0);
const String SERVICES_HOST =
    "candidate-evaluation-c4b24-default-rtdb.europe-west1.firebasedatabase.app";

Uri UriBuilder(String unencodedPath) {
  return Uri.https(SERVICES_HOST, unencodedPath + ".json");
}
