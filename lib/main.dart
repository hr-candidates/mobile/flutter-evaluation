import 'package:flutter/material.dart';
import 'package:flutter_evaluation/pages/placeholder_page.dart';
import 'package:flutter_evaluation/utils.dart';

void main() {
  runApp(TestApp());
}

class TestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Evaluation',
      theme: ThemeData(
        backgroundColor: SECONDARY_COLOR,
        scaffoldBackgroundColor: SECONDARY_COLOR,
      ),
      home: PlaceholderPage(),
    );
  }
}
