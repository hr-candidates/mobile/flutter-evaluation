# Omninext - Mobile TCC - Evaluation Test (Flutter)

Scopo del presente test è valutare l'estensione delle competenze del candidato in merito allo svolgimento di task concreti anziché
astrattamente definiti come avviene con le tecniche dell'intervista libera o del whiteboarding.

Il test è strutturato nella forma di un template già inizializzato da cui procedere per realizzare un semplice prototipo app con la tecnologia data, secondo le attività previste nella sezione 'Task' e rispettando le condizioni definite nella sezione 'Condizioni'.

La consegna va effettuata tramite tramite mail con allegato .zip all'indirizzo [recruting@omninext.it](mailto:recruting@omninext.it).

## Risorse per il test

Le risorse per lo svolgimento del test sono costituite da:
- Il template progettuale base (di seguito, 'scaffolding' ) da cui partire tramite 'clone' della presente repo git
- La *'Postman Collection'* che descrive i microservizi da chiamare per completare i 'Task' dati (la consultazione non è comunque necessaria, dato lo scaffolding)  [![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/c70b22efb08a2d405204)
- Il [Progetto Zeplin](https://zpl.io/2jgXKGW) contenente il design del prototipo da realizzare; il candidato che sia stato invitato al progetto potrà accedere dopo aver effettuato la registrazione su Zeplin (gratuita).

## Condizioni

Condizioni minime per considerare il test superato ed essere candidati al successivo colloquio tecnico-motivazionale sono:

- Svolgimento del **Task 1**;
- La consegna va effettuata tramite tramite mail con allegato .zip *perentoriamente* entro il termine dato;
- Aver **integralmente scritto da sè** il codice del progetto che si consegna;
- Non aver importato nel progetto alcuna library/framework di terze parti;

(è concesso, anche se non è ritenuto necessario, utilizzare framework/library di first e second party in aggiunta a quelli già importati nel template di base)

## Obiettivi

Il progetto deve essere consegnato entro la deadline contenuta nell'email di invito contente il link di accesso a questa codabase.
La valutazione del livello di competenza avverrà sulla base dei seguenti criteri:

- Numero dei task completati;
- Aderanza alla commessa nell'esecuzione dei task;
- Qualità e pulizia del codice;
- Applicazione di design pattern e rispetto degli standard archietturali;
- Puntuali commit locali dei progressi nello svolgimento dei task dati nel rispetto dello metodologia 'git flow';

In considerazione degli impegni dei candidati con impiego a tempo pieno, *la consegna anticipata non farà parte delle valutazione*; si invita quindi a sfruttare al massimo tutto il tempo a disposizione.

# Task

1. Al lancio dell'app, mostrare la schermata 'select_field', contente al suo interno una lista avente una cella per ogni singolo 'Field' (enum di chiavi e display values contenuto in 'models');
2. Al tap su ogni singola cella, l'app navigherà alla schermata successiva, 'select_specialization';
3. La navigazione da 'select_field' a 'select_specialization' deve iniettare la 'key' (es: 'ml') del 'Field' corrispondente alla cella selezionata;
4. La schermata di 'select_specialization' deve essere popolata asincronamente tramite API request all'endpoint 'Get Specialization', come in collection Postman;
5. Al tap su ogni singola cella, l'app navigherà alla schermata successiva, 'submit_application', iniettando la key per il Field precedentemente ritenuta & la key per la specialization prescelta dall'utente;
6. Deve essere mostrata la schermata 'submit_application', completa di text fields e button, incluse label e hint mostrate in 'submit_application_editing';
7. I valori risultanti dai text fields devono essere ritenuti per poter essere passati, congiuntamente con quanto precedentemente inettato, all'endpoint 'Create Application';
8. Il button 'Submit' non deve essere attivo se i tre campi testuali non sono tutti popolati;
9. Al tap sul button bisogna gestire la chiamata async a 'Create Application', inclusa attesa ed eventuale errore;
10. In caso di successo, l'app deve mostrare un massaggio di conferma all'utente tramite dialog come nella schermata del design 'application_sent'
11. Al tap su ok, l'app deve killarsi (suggerito 'exit')
12. All'appertura dell'app deve apparire uno splash screen come da schermata 'splash'

## Raccomandazioni finali

- Nello scaffolding sono inclusi il file 'utils', contente costanti e metodi utili, e la cartella 'models' con i data model per il parsing/invio delle API request; questo al fine di facilitare al candidato lo svolgimento del test;
- Tutti gli 'assets' rilevanti sono già inclusi nello scaffolding e accessibili
- Lo standard  architetturale di riferimento prevede l'utilizzo di StatelessWidget e Provider ma sono ammessi approcci diversi di *state management*, a patto che non dipendano da framework/library terzi (vedere *Condizioni*)

##

*Ricordati che questo piccolo esercizio ci permette di capire meglio quali siano le tue strategie di problem solving, analisi e approcio allo sviluppo, quindi non preoccuparti della quantità di task completati.*
*Un ultimo suggerimento: è più importante per noi capire come risolvi i problemi, piuttosto che ricevere tutti i task 'sommariamente' ;)*

*Happy Coding :)*
